package ru.demidovichas.openshifthelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenshifthelloworldApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenshifthelloworldApplication.class, args);
    }

}
