package ru.demidovichas.openshifthelloworld.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private final static String p = "<p>";
    private final static String p_close = "</p>";

    @RequestMapping(path = "/hello")
    public String hello() {
        return "Hello world";
    }

    @RequestMapping(path = "/")
    public String landingPage() {
        StringBuffer sb = new StringBuffer();
        addRow( sb, "This is hello world os project");
        addRow(sb, "System properties:\n");
        addRow(sb, "_______________________________________________________________");
        System.getProperties().keySet().forEach(o -> addRow(sb, o + ":" + System.getProperty(o.toString())));
        addRow(sb, "_______________________________________________________________");
        sb.append("\n System env:");
        System.getenv().entrySet().forEach(stringStringEntry -> addRow(sb, stringStringEntry.toString()));
        addRow(sb, "_______________________________________________________________");
        return sb.toString();
    }

    private void addRow(StringBuffer sb, String row) {
        sb.append(p);
        sb.append(row);
        sb.append(p_close);
    }
}
